const fs = require("fs");
const path = require("path");

function problem1(dirName, noOfFiles = parseInt(Math.random() * 10)) {
  createDirectory(dirName, (err, message) => {
    if (err) {
      console.error(err);
    } else {
      console.log(message);
      createFiles(dirName, noOfFiles, (err, message) => {
        if (err) {
          console.error(err);
        } else {
          console.log(message);
          removeFiles(dirName, (err, message) => {
            if (err) {
              console.error(err);
            } else {
              console.log(message);
              deleteDirectory(dirName, (err, message) => {
                if (err) {
                  console.error(err);
                } else {
                  console.log(message);
                }
              });
            }
          });
        }
      });
    }
  });
}

//Create Directory
function createDirectory(dirName, callback) {
  fs.mkdir(dirName, (err) => {
    if (err) {
      callback(err);
    } else {
      callback(null, "Directory Created");
    }
  });
}

//Create single File
function createFile(fileName, data, callback) {
  fs.writeFile(fileName, data, (err) => {
    callback(err);
  });
}

//Create Files
function createFiles(dirName, noOfFiles, callback) {
  let count = 0;
  for (let idx = 1; idx <= noOfFiles; idx++) {
    const fileName = path.join(dirName, `file${idx}.json`);
    const data = {
      id: idx,
      fname: `file${idx}`,
    };
    const jsonData = JSON.stringify(data);

    createFile(fileName, jsonData, (err) => {
      if (err) {
        callback(err);
      } else {
        count++;
      }

      if (count === noOfFiles) {
        callback(null, "Files created");
      }
    });
  }
}

//delete single file
function deleteFile(fileName, callback) {
  fs.unlink(fileName, (err) => {
    callback(err);
  });
}

//Delete files
function removeFiles(dirName, callback) {
  fs.readdir(dirName, (err, data) => {
    if (err) {
      callback(err);
    } else {
      let count = 0;
      for (let idx = 0; idx < data.length; idx++) {
        const fileName = path.join(dirName, data[idx]);
        deleteFile(fileName, (err) => {
          if (err) {
            callback(err);
          } else {
            count++;
          }

          if (count === data.length) {
            callback(null, "Files Deleted.");
          }
        });
      }
    }
  });
}

//Delete Directory
function deleteDirectory(dirName, callback) {
  fs.rmdir(dirName, (err) => {
    if (err) {
      callback(err);
    } else {
      callback(null, "Directory Deleted");
    }
  });
}

module.exports = problem1;