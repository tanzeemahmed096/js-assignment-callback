const fs = require("fs");

function problem2(filePath){
    readFileTxt(filePath, (err, readData) => {
        if(err){
            console.error(err);
        }else{
            // console.log(readData);
            contentToUpperCase(readData, (err, upperTxt) => {
                if(err){
                    console.error(err);
                }else{
                    // console.log(upperTxt);
                    const fileName1 = "newfile1.txt";
                    writeFileTxt(fileName1, upperTxt, (err, message) => {
                        if(err){
                            console.error(err);
                        }else{
                            // console.log(message);
                            writeFileTxt("filesnames.txt", fileName1, (err, data) => {
                                if(err){
                                    console.error(err);
                                }else{
                                    // console.log(data);
                                    readFileTxt(fileName1, (err, upperCaseData) => {
                                        if(err){
                                            console.error(err);
                                        }else{
                                            // console.log(upperCaseData);
                                            const fileName2 = "newFile2.txt"
                                            contentToLowerCaseSentence(upperCaseData, (err, lowerTxtSentence) => {
                                                if(err){
                                                    console.error(err);
                                                }else{
                                                    // console.log(lowerTxt);
                                                    const fileName2 = "newfile2.txt";
                                                    writeFileTxt(fileName2, lowerTxtSentence, (err, message) => {
                                                        if(err){
                                                            console.error(err);
                                                        }else{
                                                            // console.log(message);
                                                            appendFileTxt("filesnames.txt", fileName2, (err, message) => {
                                                                if(err){
                                                                    console.error(err);
                                                                }else{
                                                                    // console.log(message);
                                                                    readFileTxt(fileName2, (err, lowerCaseData) => {
                                                                        if(err){
                                                                            console.error(err);
                                                                        }else{
                                                                            // console.log(lowerCaseData);
                                                                            sortContent(lowerCaseData, (err, sortedData) => {
                                                                                if(err){
                                                                                    console.error(err);
                                                                                }else{
                                                                                    // console.log(sortedData);
                                                                                    const fileName3 = "newfile3.txt";
                                                                                    writeFileTxt(fileName3, sortedData, (err, message) => {
                                                                                        if(err){
                                                                                            console.error(err);
                                                                                        }else{
                                                                                            // console.log(message);
                                                                                            appendFileTxt("filesnames.txt", fileName3, (err, message) => {
                                                                                                if(err){
                                                                                                    console.error(err);
                                                                                                }else{
                                                                                                    // console.log(message);
                                                                                                    readFileTxt("filesnames.txt", (err, files) => {
                                                                                                        if(err){
                                                                                                            console.error(err);
                                                                                                        }else{
                                                                                                            // console.log(files);
                                                                                                            removeFiles(files, (err, message) => {
                                                                                                                if(err){
                                                                                                                    console.error(err);
                                                                                                                }else{
                                                                                                                    console.log(message);
                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

//Read File
function readFileTxt(filePath, callback){
    fs.readFile(filePath, "utf-8", (err, data) => {
        if(err){
            callback(err);
        }else{
            callback(null, data);
        }
    })
}

//Convert the content to uppercase
function contentToUpperCase(content,callback ){
    setTimeout(() => {
        try {
            const upperTxt = content.toUpperCase();
            callback(null, upperTxt);
        }catch(error){
            callback(error);
        }
    }, 0);
}

//Write a new file
function writeFileTxt(fileName, content, callback){
    fs.writeFile(fileName, content, (err) => {
        if(err){
            callback(err);
        }else{
            callback(null, "File Created.")
        }
    })
}

//Convert the content to lowercase sentence
function contentToLowerCaseSentence(content,callback ){
    setTimeout(() => {
        try {
            const lowerTxtsentence = content.toLowerCase().split(". ").join("\n");
            callback(null, lowerTxtsentence);
        }catch(error){
            callback(error);
        }
    }, 0);
}

//Append to a existing file
function appendFileTxt(fileName, content, callback){
    fs.appendFile(fileName, "\n" + content, (err) => {
        if(err){
            callback(err);
        }else{
            callback(null, "File Appended.")
        }
    })
}

//Sort the content
function sortContent(content, callback){
    setTimeout(() => {
        try {
            content = content.split("\n").filter(sentence => sentence).map(sentence => {
                return sentence.split(" ").sort().join(" ");
            }).sort().join("\n");
            callback(null, content);
        }catch(error){
            callback(error)
        }
    }, 0);
}

//delete single file
function deleteFile(fileName, callback) {
    fs.unlink(fileName, (err) => {
      callback(err);
    });
}

//Delete files
function removeFiles(filesnames, callback){
    let count = 0;
    filesnames = filesnames.split("\n");
    for(let idx = 0; idx < filesnames.length; idx++){
        const fileName = filesnames[idx];
        deleteFile(fileName, (err) => {
            if(err){
                callback(err);
            }else{
                count++;
            }

            if(count === filesnames.length){
                callback(null, "Files Deleted");
            }
        })
    }
}

module.exports = problem2;